from mongoengine import Document, StringField, EmbeddedDocument, ListField, EmbeddedDocumentField

class Location(EmbeddedDocument):
    location = StringField(required=True)
    department = StringField()
    status = StringField()

class Agency(EmbeddedDocument):
    agency = StringField(required=True)
    locations = ListField(EmbeddedDocumentField(Location))
    department = StringField()
    status = StringField()

class Client(Document):
    client = StringField(required=True)
    agencies = ListField(EmbeddedDocumentField(Agency))